/**
@defgroup gorandi_timer Timer_1
@note
Configuración del timer 1.
	- 1KHz Tick
	- Normal Mode
	- Overflow interruption
*/

/**@{*/

/* ----------------------------------------------------------------------- */
#ifdef _timer1_   
/* ----------------------------------------------------------------------- */

void init_Timer1(void);
void setTimeOut_Timer1(uchar timer, uint valor);
uchar timeOut_Timer1(uchar timer);
void delay_Timer1(uint tiempo);

/* ----------------------------------------------------------------------- */
#else
/* ----------------------------------------------------------------------- */

extern void init_Timer1(void);
extern void setTimeOut_Timer1(uchar timer, uint valor);
extern uchar timeOut_Timer1(uchar timer);
extern void delay_Timer1(uint tiempo);

/* ----------------------------------------------------------------------- */
#endif
/* ----------------------------------------------------------------------- */

/**************************** TIMER 1 *****************************************/

/* Base de tiempo*/
#define BASE_TIEMPO_1      1 //

/* Valor de los timeouts para la base de tiempo propuesta */
#define T1_5S      (5 /BASE_TIEMPO_1)
#define T1_20S      (20 /BASE_TIEMPO_1)
#define T1_30S      (30 /BASE_TIEMPO_1)
#define T1_60S      (60 /BASE_TIEMPO_1)
#define T1_1M      (60 /BASE_TIEMPO_1)
#define T1_4M      (240 /BASE_TIEMPO_1)
#define T1_5M      (300 /BASE_TIEMPO_1)
#define T1_1H      (3600 /BASE_TIEMPO_1)


//! Definición de los timeouts que utilizan el timer 1
enum {
	TIMEOUT,
	CANT_TIMER_1
};
// Timer 1 Configuration
//	1KHz Tick
//	Normal Mode
#define TIMER1_COUNT_H  0xC2
#define TIMER1_COUNT_L  0xF7
#define TIMER1_PRESCALER  0x05 //< Configuración del prescaler
#define TIMER1_TCCR1A 0x00 //< OCCR Disconneted
#define TIMER1_TCCR1B 0x00 //< No PWM Mode
#define TIMER1_ON (TIMER1_TCCR1B | TIMER1_PRESCALER) //< Start Timer
#define TIMER1_OFF (TIMER1_TCCR1B & ~TIMER1_PRESCALER)
#define TIMER1_SYNC_INTERNAL_MODE 0x00
#define TIMER1_INTERRUPT_ON TIMSK1 | (1<<TOIE1) //< Enable Timer Interrupt

/**************************** FIN TIMER 0 *************************************/

/**@{*/