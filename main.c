
/**
@code 
	#include "ds18s20.h" 
	#include "delay.h"
	#include <avr/io.h>
	#include <avr/interrupt.h>
	#include <stdlib.h>
	#include <ctype.h>
	#include <stdio.h>
	#include <string.h>
	#include "CTES.H"
	#include "timer0.h"
	#include "timerStructure.h"
	#include "LCD.h"
	#include "ds18s20.h"
	#include <util/delay.h>
	#include "i2cmaster.h"
	#include "mcp3424.h"
	#include "asciiTools.h"
	#include "si7021.h"
	#include "crc.h"
	#include "uart.h"
@endcode
@defgroup gorandi_main Main
@date 13/05/2017
@author Ezequiel Gorandi
@version 1.00

@brief 
	Software para manejo de un módulo prufstand.
	
Sensores utilizados:
Actuadores utilizados:

*/
/**@{*/
	
#include "main.h"

#define _MAIN_DEBUG_

#define DEVICE_ID "00001"
#define HUSO_HORARIO 2 //La hora del Datalogger = UTC + HUSO_HORARIO
#define BUFFER_LENGTH 50

#define INICIO_CADENA "$"
#define FIN_CADENA "\0"
#define SEPARADOR_ATRIBUTOS ","

	

//************************ VARIABLES GLOBALES EXTERNAS *************************
extern time_t timestamp ;



/**
 @brief Inicialización de los puertos entrada/salida.
 @param void
 @return void
*/
void port_init(void)
{
	PORTA = 0x00;
	DDRA  = 0x00;

	//El PB7 enciende el display
	PORTB = 0x71;
	DDRB  = 0xF7;

	//El puerto C y D se utilizan para el display
	PORTC = 0x00;
	DDRC  = 0xFF;

	PORTD = 0x1F;
	DDRD  = 0xF0;

	// 	El PE6 se utiliza como salida para el trigger del HS04
	// 	El PE7 se lo utiliza como entrada para provocar la interrupción en el tiemer3
	//	a traver del ICP.
	PORTE = 0x00;
	DDRE  = 0x40;

	PORTF = 0x00;
	DDRF  = 0x00;

	PORTG = 0x00;
	DDRG  = 0x00;
}


/**
 @brief Inicialización de todos los componentes del programa.
 @param void
 @return void
*/
void init_devices ( void )
{
	//stop errant interrupts until set up
	cli(); //disable all interrupts
	port_init();
//	init_Timer0();
//	init_Timer1();
	i2c_init();
	sei(); //re-enable interrupts
	uart1_init();
	uart0_init();
	hmc5883l_init();
	MCUCR = 0x00;
	//set_sleep_mode(SLEEP_MODE_IDLE);
}


/**
 @brief Main del programa
 @param void
 @return void
 @note 

*/
int main(void)
{
//************************** DEFINICIÓN DE VARIABLES ***************************
	int i = 0 ;
	MAGNETROMETRO magnetrometro ;
	char itmp[10];
	
	init_devices();

//***************************** START PROGRAM **********************************	init_devices();
	//--- Inicializacion de los timeOut  ---

	RS232_On();
	#ifdef _MAIN_DEBUG_
		uart1_txStringLb("");
		uart1_txStringLb("---------------------------------------");
		uart1_txStringLb("         Iniciando Programa");
		uart1_txStringLb("---------------------------------------");
		uart1_txStringLb("");
	#endif
//****************************** START LOOP ************************************
	while (1) 
	{		
//************************** TIMEOUTS DE SENSORES ******************************
		hmc5883l_getRawData(&magnetrometro);
		hmc5883l_getData(&magnetrometro);

		itoa(magnetrometro.mxraw, itmp, 10); 
		uart1_txString(itmp); 
		uart1_txString(" ");
		
		itoa(magnetrometro.myraw, itmp, 10);
		uart1_txString(itmp);
		uart1_txString(" ");
		
		itoa(magnetrometro.mzraw, itmp, 10);
		uart1_txString(itmp);
		uart1_txString(" ");
		
		uart1_txStringLb("");
		uart1_txStringLb("");
/*		
		dtostrf(magnetrometro.mx, 3, 5, itmp); 
		uart1_txString(itmp); 
		uart1_txString("");
		dtostrf(magnetrometro.my, 3, 5, itmp); 
		uart1_txString(itmp); 
		uart1_txString("");
		dtostrf(magnetrometro.mz, 3, 5, itmp); 
		uart1_txString(itmp); 
		uart1_txString("");
*/		
		_delay_ms(500);
	}

/*
	//Low Power mode
	PRR0 = 0xD5;
	PRR1 = 0xFE;
	cli();
	sleep_enable();
	sei();
	sleep_cpu();
	sleep_disable();
	PRR0 = 0x00;
	PRR1 = 0x00;*/
    
}
/**@{*/
