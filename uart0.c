/*
UART.c
2013 - Josh Ashby
joshuaashby@joshashby.com
http://joshashby.com
http://github.com/JoshAshby
freenode/#linuxandsci - JoshAshby
*/

/*------------------------------------------------------------------------*/
#define _UART_
/*------------------------------------------------------------------------*/

//-------------------------------------------
#include "main.h"

uchar UART_RxLlen;
uchar UART_RxVaci;
uchar UART_TxLlen;
uchar UART_TxVaci;
uchar UART_TxActivo;
uchar UART_TxBuf[UART_TX_BUFFER_SIZE];
uchar UART_RxBuf[UART_RX_BUFFER_SIZE];

/**************************************************************************
 Prototipo: void UART_RX1_interrupt(void)
 Objetivo:  atiende la interrupcion de caracter recibido
 Recibe:
 Devuelve:
 Notas:
 **************************************************************************/

ISR(USART0_RX_vect)
{
   uchar dato;
   uchar indice;

   /* veo que no haya no error de trama ni de sobreescritura */
   if((UCSR0A & (1<<FE0)) || (UCSR0A & (1<<DOR0)) || (UCSR0A & (1<<PE0)) )
   {
      /* leo lo que recibi*/
      dato = UDR0;
   }
   else
   {
      /* leo lo que recibi*/
      dato = UDR0;
      /* calculo el indice nuevo y pego la vuelta si es necesario */
      indice = (UART_RxLlen + 1) & UART_RX_BUFFER_MASK;
      /* actualizo el indice */
      UART_RxLlen = indice;

      /*  Esto deberia contener el prodedimiento de bhffer overflow */
  /*    
      if (indice == UART_RxVaci)
      {
	  dato = UDR0; 				  // descarto el dato nuevo
      }
      else
*/
      /* escribo el dato recien recibido y de esta manera borro la flag de 
         interrupcion */
      UART_RxBuf[indice] = dato;
   }
  
}
/**************************************************************************/
/**************************************************************************
 Prototipo: void UART_TX_interrupt(void)
 Objetivo:  atiende la interrupcion de caracter recibido
 Recibe:
 Devuelve:
 Notas:
 **************************************************************************/
ISR(USART0_TX_vect)
{
   uchar indice;

   /* veo si todos los datos estan transmitidos */
   if (UART_TxLlen != UART_TxVaci)
   {
      /* calculo el indice nuevo y pego la vuelta si es necesario */
      indice = (UART_TxVaci + 1) & UART_TX_BUFFER_MASK;
      /* actualizo el indice */
      UART_TxVaci = indice;
      /* saco el dato y de esta manera borro la flag de interrupcion */
      UDR0 = UART_TxBuf[indice];
   }
   else
   {
      /* si todos los datos estan transmitidos aviso */
      UART_TxActivo = FALSE;
   }
}
/**************************************************************************/
/**************************************************************************
 Prototipo: void InitUART(uint divisor)
 Objetivo:  iniciliazar la UART y los buffers
 Recibe:    
 Devuelve:
 Notas:
 **************************************************************************/
void uart0_init(void)
{
   uchar dato;
   /* deshabilito todo al setear */
   UCSR0B  = 0x00;
   /* selecciono la velocidad */
   UBRR0L = BAUD_PRESCALE; //set the baud to 9600, have to split it into the two registers
   UBRR0H = (BAUD_PRESCALE >> 8); //high end of baud register
   /* inicilizo indices de buffers */
   UART_RxVaci = UART_RxLlen = UART_TxVaci = UART_TxLlen = 0;
   /* aviso que el transmisor no esta activo */
   UART_TxActivo = FALSE;
   /* leo el UDR por si hay algua flag prendida */
   dato = UDR0;
   /* sigo con las configuraciones, sin paridad, 8 bits de datos*/
   UCSR0C = ((1<<UCSZ00) | (1<<UCSZ01)/* | (1<<URSEL)*/);
   /* habilito la recepcion y la transmision  y las interrupciones */
   UCSR0B = ((1<<RXEN0)| (1<<TXEN0) | (1<<RXCIE0) | /*(1<<UDRIE0)*/(1<<TXCIE0));
}
/**************************************************************************/

/**************************************************************************
 Prototipo: uchar RxByte(void)
 Objetivo:  lee un byte del buffer de recepcion si esta disponible o espera
            a que lo este
 Recibe:
 Devuelve:
 Notas:
 **************************************************************************/
uchar uart0_rxByte(void)
{
   uchar indice;

   /* espero a que llegue un dato */
   while (UART_RxLlen == UART_RxVaci)asm("wdr");
   /* calculo el indice nuevo y pego la vuelta si es necesario */
   indice = (UART_RxVaci + 1) & UART_RX_BUFFER_MASK;
   /* actualizo el indice */
   UART_RxVaci = indice;
   /* devuelvo el dato */
   return UART_RxBuf[indice];
}
/**************************************************************************/
/**************************************************************************
 Prototipo: void TxByte(uchar dato)
 Objetivo:  escribe un bite en el buffer de transmision si hay lugar o espera
            a que haya
 Recibe:
 Devuelve:
 Notas:
 **************************************************************************/
void uart0_txByte(uchar dato)
{
   uchar indice;

   if(UART_TxActivo)
   {
      /* calculo el indice nuevo y pego la vuelta si es necesario */
      indice = (UART_TxLlen + 1) & UART_TX_BUFFER_MASK; 
      /* espero a que haya lugar */
      while (indice == UART_TxVaci)asm("wdr");
      /* escribo el dato en el buffer */
      UART_TxBuf[indice] = dato;
      /* actualizo el indice */
      UART_TxLlen = indice;
   }
   else
   {
      /* aviso que se activo la transmision */
      UART_TxActivo = TRUE;
      /* escribo el dato a transmitir en el registro */
      UDR0 = dato;
   }
}
/**************************************************************************/
/**************************************************************************
 Prototipo: uchar DatoEnRxBuf(void) 
 Objetivo:  devuelve 1 si hay datos para leer o cero si no
 Recibe:
 Devuelve:
 Notas:
 **************************************************************************/
uchar uart0_datoEnRxBuf(void)
{
   return((UART_RxLlen != UART_RxVaci)); 
}
/**************************************************************************/


/**************************************************************************
 Prototipo: 
 Objetivo:  transmitir un string terminado en el NULL
 Recibe:    puntero al string
 Devuelve:
 Notas:
 **************************************************************************/
void uart0_txString(char *s)
{
 while(*s)
 	uart0_txByte(*s++);   
}

void uart0_txStringLb(char *s)
{
	while(*s)
	uart0_txByte(*s++);
	uart0_txByte(13);
	uart0_txByte(10);
}

void uart0_flush()
{
	while(uart0_datoEnRxBuf())
	uart0_rxByte();
}
