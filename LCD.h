/*
LCD lib V1.0  
 Do this before calling Init_LCD();
 */
/**
@defgroup gorandi_lcd Lcd Display Library
*/ 
/**@{*/
 //! Eneumeración de las pantallas utilizadas por el display LCD.
 enum 
 {
	 PANTALLA_TEMPERATURA_1 = 0,
	 PANTALLA_HUMEDAD,
	 PANTALLA_ADC,
	 PANTALLA_HORA,
	 PANTALLA_ID,
	 CANTIDAD_PANTALLAS ///< Cantidad total de pantallas definidas
 };
 
 
/* LCD data bus, 8 bit mode 
 */
extern volatile unsigned char *LCD_DIR_PORT;			// default DDRA
extern volatile unsigned char *LCD_IP_PORT;			// default PINA
extern volatile unsigned char *LCD_OP_PORT;			// default PORTA
/* LCD Enable Port
 */
extern volatile unsigned char *LCD_EN_PORT;			// default PORTC 
extern char LCD_EN_BIT;				// default BIT(7)
/* LCD Register Select Port
 */
extern volatile unsigned char *LCD_RS_PORT;			// default PORTC
extern char LCD_RS_BIT;				// default BIT(6)
/* LCD Read Write Port
 */
extern volatile unsigned char *LCD_RW_PORT;			// default PORTC
extern char LCD_RW_BIT;				// default BIT(5)


// *** LCD Function *** //

void Init_LCD(void);

void LCD_Display_Off(void);
void LCD_Display_On(void);
void LCD_Clear(void);
void LCD_Home(void);
void LCD_Cursor(char row, char column);

void LCD_Cursor_On(void);
void LCD_Cursor_Off(void);
void LCD_DisplayCharacter(char Char);

void LCD_DisplayString_F(char row, char column, const char *string);
void LCD_DisplayString(char row, char column, char *string);

void lcdOn();
void lcdOff();

/**@{*/

