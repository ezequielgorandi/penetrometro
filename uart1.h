//-------------------------------------------
//Macros (defines)
//-------------------------------------------
#define BAUD_1 9600
#define BUFF_LEN_1 700
#define BAUD_PRESCALE_1 (((F_CPU / (BAUD_1 * 16UL))) - 1)

/* Tama�os de los buffers - siempre potencias de 2 */
#define UART1_RX_BUFFER_SIZE 64 /* 1,2,4,8,16,32,64,128 o 256 bytes */
#define UART1_RX_BUFFER_MASK ( UART1_RX_BUFFER_SIZE - 1 )
#define UART1_TX_BUFFER_SIZE 128 /* 1,2,4,8,16,32,64,128 o 256 bytes */
#define UART1_TX_BUFFER_MASK ( UART1_TX_BUFFER_SIZE - 1 )

// asignacion de constantes
#define ENTER_1 '\r'

/*------------------------------------------------------------------------*/
#ifdef _UART_
/*------------------------------------------------------------------------*/

void uart1_init(void);
uchar uart1_rxByte(void);
void uart1_txByte(uchar dato);
uchar uart1_datoEnRxBuf(void);
//void TxByteScaped(uchar);
void uart1_txString(char *s);
void uart1_txStringLb(char *s);
void uart1_flush(void);
/*------------------------------------------------------------------------*/
#else
/*------------------------------------------------------------------------*/
extern void uart1_flush(void);
extern void uart1_init(void);
extern uchar uart1_rxByte(void);
extern void uart1_txByte(uchar dato);
extern uchar uart1_datoEnRxBuf(void);
//extern void TxByteScaped(uchar);
extern void uart1_txString(char *s);
extern void uart1_txStringLb(char *s);
/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/