/*
 * hardDriver_FonarsecP18R00.c
 *
 * Created: 08/06/2017 19:53:26
 *  Author: egorandi
 */ 

#include "main.h"
/**************************************************************************
 Prototipo: void RS232_On (void)
 Objetivo:  Rutina de encendido del RS232 y multiplexado
 Recibe:
 Devuelve:
 Notas:
 **************************************************************************/
void RS232_On(void)
{
//	extern uchar UART1_TxActivo;

//	while (UART1_TxActivo) asm("wdr");				//espera a que salga todo por el serie
	RS232_DDR |= (1<<P_RS232);			// lo prendo y habilito la salida por RS232
	RS232_PORT &= ~(1<<P_RS232);			// lo prendo y habilito la salida por RS232
	_delay_ms(500);
}
