/*
 * hard.h
 *
 * Created: 08/06/2017 20:31:35
 *  Author: egorandi
 */ 


/*
 * hardDriver_FonarsecP18R00.h
 *
 * Created: 08/06/2017 19:53:55
 *  Author: egorandi
 */ 

//---------------------------------------- PORT D -------------------------------------------------------//
#define RS232_PORT     	PORTD
#define RS232_DDR      	DDRD
#define RS232_IN      	PIND
#define P_RS232   	  	PORTD4
//------------------------------------- FIN PORT D -----------------------------------------------------//

/*
//---------------------------------------- PORT G -------------------------------------------------------//
#define POWER_OUT 		PORTG
#define POWER_IN      	PING
#define P_POWER   	  	PORTG0
//------------------------------------- FIN PORT G -----------------------------------------------------//
*/

/*
//----------------------------  PUERTOS DE PULSADORES  -------------------------------------//
#define PUL_OUT   	PORTD
#define PUL_IN      PIND
#define PUL_ENT		PORTD7
#define PUL_SEL		PORTD6
//----------------------------  PUERTOS DE PULSADORES  -------------------------------------//


//----------------------  PUERTOS PARA EL HS04 (Ultrasonido)  ------------------------------//
#define HS04_OUT  	PORTE
#define HS04_IN     PINE
#define HS04_TRIG	PORTE6
*/

void RS232_On(void);