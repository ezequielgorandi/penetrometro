
/**
@defgroup gorandi_main Main
*/
#define F_CPU 8000000UL	
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <avr/sleep.h>
#include <util/delay.h>
#include "hmc5883l.h"
#include "hard.h"
#include "CTES.H"
#include "timer0.h"
#include "timerStructure.h"
#include "i2cmaster.h"
#include "asciiTools.h"
#include "uart0.h"
#include "uart1.h"
#include "time.h"
#include "stringTools.h"
#include "LCD.h"


