/**
@date 24/05/2017
@defgroup gorandi_timer Timer_0
@note
Microcontrolador: Atmega128
Configuración del timer 0.
	- 1KHz Tick
	- Normal Mode
	- Overflow interruption
*/

/**@{*/

/* ----------------------------------------------------------------------- */
#ifdef _timer0_   
/* ----------------------------------------------------------------------- */

void init_Timer0(void);
void setTimeOut_Timer0(uchar timer, uint valor);
uchar timeOut_Timer0(uchar timer);
void delay_Timer0(uint tiempo);

/* ----------------------------------------------------------------------- */
#else
/* ----------------------------------------------------------------------- */

extern void init_Timer0(void);
extern void setTimeOut_Timer0(uchar timer, uint valor);
extern uchar timeOut_Timer0(uchar timer);
extern void delay_Timer0(uint tiempo);

/* ----------------------------------------------------------------------- */
#endif
/* ----------------------------------------------------------------------- */

/**************************** TIMER 0 *****************************************/

/* Base de tiempo*/
#define BASE_TIEMPO_0      1

/* Valor de los timeouts para la base de tiempo propuesta */
//#define T0_1MS	   (1 / BASE_TIEMPO_0)
/*#define T0_10MS    (10 / BASE_TIEMPO_0)
#define T0_15MS    (15 / BASE_TIEMPO_0)
#define T0_20MS    (20 / BASE_TIEMPO_0)
#define T0_25MS    (25 / BASE_TIEMPO_0)
#define T0_30MS    (30 / BASE_TIEMPO_0)
#define T0_40MS    (40 / BASE_TIEMPO_0)
#define T0_50MS    (50 / BASE_TIEMPO_0)
#define T0_60MS    (50 / BASE_TIEMPO_0)
#define T0_100MS   (100 / BASE_TIEMPO_0)
#define T0_200MS   (200 / BASE_TIEMPO_0)
#define T0_250MS   (250 / BASE_TIEMPO_0)*/
#define T0_300MS   (300 / BASE_TIEMPO_0)
#define T0_400MS   (400 / BASE_TIEMPO_0)
#define T0_500MS   (500 / BASE_TIEMPO_0)
#define T0_800MS   (800 / BASE_TIEMPO_0)
#define T0_1S      (1000 / BASE_TIEMPO_0)
#define T0_2S      (2000 / BASE_TIEMPO_0)
#define T0_3S      (3000 / BASE_TIEMPO_0)
#define T0_5S      (5000 / BASE_TIEMPO_0)
#define T0_8S      (8000 / BASE_TIEMPO_0)
#define T0_10S      (10000 / BASE_TIEMPO_0)
#define T0_60S      (60000 /(uint32_t)BASE_TIEMPO_0)


//! Definición de los timeouts que utilizan el timer 0
enum {
	DELAY_0 = 0,
	CANT_TIMER_0
};
// Timer 0 Configuration
//	1KHz Tick
//	Normal Mode
//#define TIMER0_COUNT  0x64 //
//#define TIMER0_PRESCALER  0x05 //< Configuración del prescaler
/*
#define TIMER0_COUNT  0x06 //
#define TIMER0_PRESCALER  0x03 //< Configuración del prescaler
*/


#define TIMER0_COUNT  0x83;  //
#define TIMER0_PRESCALER  0x04;  //< Configuración del prescaler


#define TIMER0_TCCR0 0x00 //< OCCR Disconneted
#define TIMER0_TCCR0 0x00 //< No PWM Mode
#define TIMER0_ON (TIMER0_TCCR0 | TIMER0_PRESCALER) //< Start Timer
#define TIMER0_OFF (TIMER0_TCCR0 & ~TIMER0_PRESCALER)
#define TIMER0_SYNC_INTERNAL_MODE 0x00
#define TIMER0_INTERRUPT_ON TIMSK | (1<<TOIE0) //< Enable Timer Interrupt

/**************************** FIN TIMER 0 *************************************/

/**@{*/