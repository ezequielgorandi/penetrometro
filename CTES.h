/*------------------------------------------------------------------------*/
#ifndef _CTES_
#define _CTES_
/*------------------------------------------------------------------------*/

#define TRUE       1
#define FALSE      (!TRUE)
#define NEW_LINE "\n\r"

#define ERROR 0
#define OK 1

/* Usados por comodidad */
typedef unsigned char byte;
typedef unsigned char uchar;
typedef unsigned int  uint;
typedef unsigned long ulong;
#endif