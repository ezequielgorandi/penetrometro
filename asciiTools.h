/*
 * asciiTools.h
 *
 * Created: 16/01/2017 08:41:08 p. m.
 *  Author: Ezequiel
 */ 

void uint32ToAscii (uint32_t, char*);
void int32ToAscii (int32_t, char*);
void intToAscii (int, char*);
void uintToAscii(uint16_t, char*);
void uintToAsciiGPRS (uint16_t, char* );
