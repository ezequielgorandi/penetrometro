//-------------------------------------------
//Macros (defines)
//-------------------------------------------
#define BAUD 9600
#define BUFF_LEN 700
#define BAUD_PRESCALE (((F_CPU / (BAUD * 16UL))) - 1)

/* Tama�os de los buffers - siempre potencias de 2 */
#define UART_RX_BUFFER_SIZE 64 /* 1,2,4,8,16,32,64,128 o 256 bytes */
#define UART_RX_BUFFER_MASK ( UART_RX_BUFFER_SIZE - 1 )
#define UART_TX_BUFFER_SIZE 128 /* 1,2,4,8,16,32,64,128 o 256 bytes */
#define UART_TX_BUFFER_MASK ( UART_TX_BUFFER_SIZE - 1 )

// asignacion de constantes
#define ENTER '\r'

/*------------------------------------------------------------------------*/
#ifdef _UART_
/*------------------------------------------------------------------------*/
void uart0_flush(void);
void uart0_init(void);
uchar uart0_rxByte(void);
void uart0_txByte(uchar dato);
uchar uart0_datoEnRxBuf(void);
//void TxByteScaped(uchar);
void uart0_txString(char *s);
void uart0_txStringLb(char *s);

/*------------------------------------------------------------------------*/
#else
/*------------------------------------------------------------------------*/
extern void uart0_flush(void);
extern void uart0_init(void);
extern uchar uart0_rxByte(void);
extern void uart0_txByte(uchar dato);
extern uchar uart0_datoEnRxBuf(void);
//extern void TxByteScaped(uchar);
extern void uart0_txString(char *s);
extern void uart0_txStringLb(char *s);
/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/