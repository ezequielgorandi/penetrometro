/*
 * timer1.c
 *
 * Created: 20/02/2017 07:08:10 p. m.
 *  Author: Ezequiel
 */ 
/**
@defgroup gorandi_timer Timer_0
@date 12/12/2016.
@author Ezequiel Gorandi
@version 1.0
@code
#include "timer1.h"
@endcode
@brief
Librer�a para la utilizaci�n del timer 1 del uC Atmega2560
*/


/**@{*/

#define _timer1_
#include "main.H"


TIMERS_INTERNOS Timers_1[CANT_TIMER_1];
time_t timestamp = 0;

/**
@brief   Atenci�n de la interrupci�n por overflow del timer 1.
@note    
   - Actualiza los timeouts definidos.
   - Recarga la cuenta del timer 1
*/
ISR(TIMER1_OVF_vect)
{
	uchar i;
	/* recargo las cuentas */
	TCNT1H = TIMER1_COUNT_H; //Set Count
	TCNT1L = TIMER1_COUNT_L;
	/* decremento los timeouts */
	
	timestamp++;
	for (i = 0; i < CANT_TIMER_1; i++)
		if(Timers_1[i].Tiempo) Timers_1[i].Tiempo--;
}


/**
@brief   Inicializaci�n del timer 1.
@param   void
@return  void
@note    Configura el timer con los par�metros	definidos en timero.h
*/
void init_Timer1(void)
{

   uchar i;

   TCCR1B = TIMER1_TCCR1B; //Timer 1 Stop
   ASSR  = TIMER1_SYNC_INTERNAL_MODE; //Sync mode. Internal Clock
   TCNT1H = TIMER1_COUNT_H; //Set Count
   TCNT1L = TIMER1_COUNT_L;
   TIMSK1 = TIMER1_INTERRUPT_ON; //Enable Timer Interrupt

// All the Timer1 TimeOut = 0
   for(i = 0; i < CANT_TIMER_1; i++) 
   {
      Timers_1[i].Tiempo = 0;
      Timers_1[i].En_uso = 0;
   }
   
   TCCR1B = TIMER1_ON; //Start Timer
}

/**
@brief   Carga un valor determinado en un timerout.
@param   uchar : timeout que se desea settear.
@param   uchar : valor con el cual desea settearse el timeout.
@return  void
@note    Los timeouts utilizados por el programa, as� como los valores con los
         que estos pueden ser setteados, son definidos en timer1.h
*/
void setTimeOut_Timer1(uchar timer, uint valor)
{
   /* cargo el valor de tiempo */
   Timers_1[timer].Tiempo = valor;
   /* activo el uso del timer */
   Timers_1[timer].En_uso = TRUE;
}
/**
@brief   Verifica si se ha vencido un determinado timeout
@param   uchar : timeout que se desea verificar.
@retval  uchar TRUE : Hubo timeout
@retval  uchar FALSE : A�n no hubo timeout
@note    
   - Los timeouts utilizados por el programa, son definidos en timer1.h
   - Previamente debe settearse el timeout utilizando 
     @codevoid setTimeOut_Timer1(uchar timer, uint valor)@endcode
   - Una vez ocurrido el timeout, debe settearse nuevamente esta funcion si se
     quiere que vuelva a ocurrir.
*/
uchar timeOut_Timer1(uchar timer)
{
   if(Timers_1[timer].En_uso && !Timers_1[timer].Tiempo)
   {
      Timers_1[timer].En_uso = FALSE;
      return TRUE;
   }
   return FALSE;
}