/*
UART.c
2013 - Josh Ashby
joshuaashby@joshashby.com
http://joshashby.com
http://github.com/JoshAshby
freenode/#linuxandsci - JoshAshby
*/

/*------------------------------------------------------------------------*/
#define _UART1_
/*------------------------------------------------------------------------*/

//-------------------------------------------
#include "main.h"

uchar UART1_RxLlen;
uchar UART1_RxVaci;
uchar UART1_TxLlen;
uchar UART1_TxVaci;
uchar UART1_TxActivo;
uchar UART1_TxBuf[UART1_TX_BUFFER_SIZE];
uchar UART1_RxBuf[UART1_RX_BUFFER_SIZE];

/**************************************************************************
 Prototipo: void UART1_RX1_interrupt(void)
 Objetivo:  atiende la interrupcion de caracter recibido
 Recibe:
 Devuelve:
 Notas:
 **************************************************************************/

ISR(USART1_RX_vect)
{
   uchar dato;
   uchar indice;

   /* veo que no haya no error de trama ni de sobreescritura */
   if((UCSR1A & (1<<FE1)) || (UCSR1A & (1<<DOR1)) || (UCSR1A & (1<<PE1)) )
   {
      /* leo lo que recibi*/
      dato = UDR1;
   }
   else
   {
      /* leo lo que recibi*/
      dato = UDR1;
      /* calculo el indice nuevo y pego la vuelta si es necesario */
      indice = (UART1_RxLlen + 1) & UART1_RX_BUFFER_MASK;
      /* actualizo el indice */
      UART1_RxLlen = indice;

      /*  Esto deberia contener el prodedimiento de bhffer overflow */
  /*    
      if (indice == UART1_RxVaci)
      {
	  dato = UDR1; 				  // descarto el dato nuevo
      }
      else
*/
      /* escribo el dato recien recibido y de esta manera borro la flag de 
         interrupcion */
      UART1_RxBuf[indice] = dato;
   }
  
}
/**************************************************************************/
/**************************************************************************
 Prototipo: void UART1_TX_interrupt(void)
 Objetivo:  atiende la interrupcion de caracter recibido
 Recibe:
 Devuelve:
 Notas:
 **************************************************************************/
ISR(USART1_TX_vect)
{
   uchar indice;

   /* veo si todos los datos estan transmitidos */
   if (UART1_TxLlen != UART1_TxVaci)
   {
      /* calculo el indice nuevo y pego la vuelta si es necesario */
      indice = (UART1_TxVaci + 1) & UART1_TX_BUFFER_MASK;
      /* actualizo el indice */
      UART1_TxVaci = indice;
      /* saco el dato y de esta manera borro la flag de interrupcion */
      UDR1 = UART1_TxBuf[indice];
   }
   else
   {
      /* si todos los datos estan transmitidos aviso */
      UART1_TxActivo = FALSE;
   }
}
/**************************************************************************/
/**************************************************************************
 Prototipo: void InitUART(uint divisor)
 Objetivo:  iniciliazar la UART y los buffers
 Recibe:    
 Devuelve:
 Notas:
 **************************************************************************/
void uart1_init(void)
{
   uchar dato;
   /* deshabilito todo al setear */
   UCSR1B  = 0x00;
   /* selecciono la velocidad */
   UBRR1L = BAUD_PRESCALE_1; //set the baud to 9600, have to split it into the two registers
   UBRR1H = (BAUD_PRESCALE_1 >> 8); //high end of baud register
   /* inicilizo indices de buffers */
   UART1_RxVaci = UART1_RxLlen = UART1_TxVaci = UART1_TxLlen = 0;
   /* aviso que el transmisor no esta activo */
   UART1_TxActivo = FALSE;
   /* leo el UDR por si hay algua flag prendida */
   dato = UDR1;
   /* sigo con las configuraciones, sin paridad, 8 bits de datos*/
   UCSR1C = ((1<<UCSZ10) | (1<<UCSZ11)/* | (1<<URSEL)*/);
   /* habilito la recepcion y la transmision  y las interrupciones */
   UCSR1B = ((1<<RXEN1)| (1<<TXEN1) | (1<<RXCIE1) | /*(1<<UDRIE1)*/(1<<TXCIE1));
}
/**************************************************************************/


void uart1_flush()
{
	while(uart1_datoEnRxBuf())
		uart1_rxByte();
}

/**************************************************************************
 Prototipo: uchar RxByte(void)
 Objetivo:  lee un byte del buffer de recepcion si esta disponible o espera
            a que lo este
 Recibe:
 Devuelve:
 Notas:
 **************************************************************************/
uchar uart1_rxByte(void)
{
	uchar indice;
	if (uart1_datoEnRxBuf())
	{
		/* espero a que llegue un dato */
		while (UART1_RxLlen == UART1_RxVaci)asm("wdr");
		/* calculo el indice nuevo y pego la vuelta si es necesario */
		indice = (UART1_RxVaci + 1) & UART1_RX_BUFFER_MASK;
		/* actualizo el indice */
		UART1_RxVaci = indice;
		/* devuelvo el dato */
		return UART1_RxBuf[indice];
	}
	else
		return 0 ;
}
/**************************************************************************/
/**************************************************************************
 Prototipo: void TxByte(uchar dato)
 Objetivo:  escribe un bite en el buffer de transmision si hay lugar o espera
            a que haya
 Recibe:
 Devuelve:
 Notas:
 **************************************************************************/
void uart1_txByte(uchar dato)
{
   uchar indice;

   if(UART1_TxActivo)
   {
      /* calculo el indice nuevo y pego la vuelta si es necesario */
      indice = (UART1_TxLlen + 1) & UART1_TX_BUFFER_MASK; 
      /* espero a que haya lugar */
      while (indice == UART1_TxVaci)asm("wdr");
      /* escribo el dato en el buffer */
      UART1_TxBuf[indice] = dato;
      /* actualizo el indice */
      UART1_TxLlen = indice;
   }
   else
   {
      /* aviso que se activo la transmision */
      UART1_TxActivo = TRUE;
      /* escribo el dato a transmitir en el registro */
      UDR1 = dato;
   }
}
/**************************************************************************/
/**************************************************************************
 Prototipo: uchar DatoEnRxBuf(void) 
 Objetivo:  devuelve 1 si hay datos para leer o cero si no
 Recibe:
 Devuelve:
 Notas:
 **************************************************************************/
uchar uart1_datoEnRxBuf(void)
{
   return((UART1_RxLlen != UART1_RxVaci)); 
}
/**************************************************************************/


/**************************************************************************
 Prototipo: 
 Objetivo:  transmitir un string terminado en el NULL
 Recibe:    puntero al string
 Devuelve:
 Notas:
 **************************************************************************/
void uart1_txString(char *s)
{
 while(*s)
 	uart1_txByte(*s++);   
}

/**************************************************************************
 Prototipo: 
 Objetivo:  transmitir un string terminado en el NULL
 Recibe:    puntero al string
 Devuelve:
 Notas:
 **************************************************************************/
void uart1_txStringLb(char *s)
{
 while(*s)
 	uart1_txByte(*s++);
 uart1_txByte(13);
 uart1_txByte(10);   
}