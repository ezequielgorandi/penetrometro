/**
@defgroup gorandi_lcd Lcd Display Library
@endcode
@date 15/01/2017.
@version 1.0

@brief
Librería para la utilización de display LCD

-Librerías requeridas:
@code
	- LCD.h
@endcode
*/


#include "main.H"
// ***** Define I/O pins ***** //

#define LCD_POWER_DDR DDRB
#define LCD_POWER_PORT PORTB
#define LCD_POWER PORTB7

#define BIT7 0x80
#define BIT6 0x40
#define BIT5 0x20
#define BIT4 0x10
#define BIT3 0x08
#define BIT2 0x04
#define BIT1 0x02
#define BIT0 0x01

volatile unsigned char *LCD_EN_PORT = &PORTD;
volatile unsigned char *LCD_DIR_PORT = &DDRC;
volatile unsigned char *LCD_IP_PORT = &PINC;
volatile unsigned char *LCD_OP_PORT = &PORTC;
volatile unsigned char *LCD_RS_PORT = &PORTD;
volatile unsigned char *LCD_RW_PORT = &PORTG;

/*Por esta configuración, solo puede utilizarse con todos los comandos en un mismo puerto*/
volatile unsigned char *LCD_CMD_DIR_PORT1 = &DDRG;
volatile unsigned char *LCD_CMD_DIR_PORT2 = &DDRD;




char LCD_EN_BIT = BIT7;
char LCD_RS_BIT = BIT5;
char LCD_RW_BIT = BIT6;

//#define SET_LCD_CMD_PORT_OUTPUT  *LCD_CMD_DIR_PORT |= LCD_EN_BIT|LCD_RS_BIT|LCD_RW_BIT
#define SET_LCD_CMD_PORT_OUTPUT1  *LCD_CMD_DIR_PORT1 |= LCD_EN_BIT|LCD_RW_BIT
#define SET_LCD_CMD_PORT_OUTPUT2  *LCD_CMD_DIR_PORT2 |= LCD_RS_BIT
#define SET_LCD_E      	        *LCD_EN_PORT |= LCD_EN_BIT
#define CLEAR_LCD_E       	    *LCD_EN_PORT &= ~LCD_EN_BIT

#define SET_LCD_DATA      	    *LCD_RS_PORT |= LCD_RS_BIT
#define SET_LCD_CMD       	    *LCD_RS_PORT &= ~LCD_RS_BIT

#define SET_LCD_READ      	    *LCD_RW_PORT |= LCD_RW_BIT
#define SET_LCD_WRITE      	    *LCD_RW_PORT &= ~LCD_RW_BIT


#define LCD_ON				0x0C   
#define LCD_CURS_ON			0x0D
#define LCD_OFF				0x08
#define LCD_HOME			0x02  
#define LCD_CLEAR			0x01
#define LCD_NEW_LINE		0xC0

#define LCD_FUNCTION_SET	0x38
#define LCD_MODE_SET		0x06

 //*****************************************************//
 // This routine will return the busy flag from the LCD //
 //*****************************************************//
/**@{*/


void lcdOn ()
{
	LCD_POWER_DDR |= (1<<LCD_POWER); //output
	LCD_POWER_PORT &= ~(1<<LCD_POWER); //low
	DDRC = 0xFF;
}

void lcdOff ()
{
	DDRC = 0x00;
	LCD_POWER_DDR |= (1<<LCD_POWER); //output
	LCD_POWER_PORT |= (1<<LCD_POWER); //low
	
	*LCD_CMD_DIR_PORT1 &= ~LCD_EN_BIT;
	*LCD_CMD_DIR_PORT1 &= ~LCD_RW_BIT ;
	*LCD_CMD_DIR_PORT2 &= ~LCD_RS_BIT ;	
}

unsigned char LCD_Busy ( void )
{
	unsigned char temp;

	CLEAR_LCD_E;	   				 // Disable LCD

	*LCD_DIR_PORT = 0x00;			// Make I/O Port input

	SET_LCD_READ;					// Set LCD to READ
	SET_LCD_CMD;					// Set LCD to command

	SET_LCD_E;	   				 

	asm("nop");

	temp = *LCD_IP_PORT;				// Load data from port

	CLEAR_LCD_E;	   				 // Disable LCD

	return temp;					 // return busy flag
}

// ********************************************** //
// *** Write a control instruction to the LCD *** //
// ********************************************** //

void LCD_WriteControl (unsigned char CMD)
{
uchar i;
	SET_LCD_CMD_PORT_OUTPUT1;
	SET_LCD_CMD_PORT_OUTPUT2;
	//while (LCD_Busy()& 0X80) asm("wdr");	// Test if LCD busy
	for (i=0;i<254;i++)
	{
		if(LCD_Busy() & 0X80) asm("wdr"); 		// Test if LCD Busy
		else break;
	}	
	CLEAR_LCD_E;	   			// Disable LCD

	SET_LCD_WRITE ;				// Set LCD to write
	SET_LCD_CMD;				// Set LCD to command

	*LCD_DIR_PORT = 0xFF;		// LCD port output
	
	*LCD_OP_PORT = CMD;			// Load data to port

	SET_LCD_E;	   				// Write data to LCD

	asm("nop");
	
	CLEAR_LCD_E;	   			// Disable LCD
}

// ***************************************** //
// *** Write one byte of data to the LCD *** //
// ***************************************** //

void LCD_WriteData (unsigned char Data)
{
 uchar i;
	SET_LCD_CMD_PORT_OUTPUT1;
	SET_LCD_CMD_PORT_OUTPUT2;
	for (i=0;i<254;i++)
	{
		if(LCD_Busy() & 0X80) asm("wdr"); 		// Test if LCD Busy
		else break;
	}
	CLEAR_LCD_E;	   				// Disable LCD
	SET_LCD_WRITE ;					// Set LCD to write
	SET_LCD_DATA;					// Set LCD to data

	*LCD_DIR_PORT = 0xFF;			// LCD port output	
	*LCD_OP_PORT = Data;				// Load data to port

	SET_LCD_E;	   				 	// Write data to LCD

	asm("nop");

	CLEAR_LCD_E;	   				 // Disable LCD
}


/**
@brief Display initialization.
@param  void
@retval void
*/	
void Init_LCD(void)
{ 
	 lcdOn();
	 _delay_ms(250);
	LCD_WriteControl (LCD_FUNCTION_SET);
	LCD_WriteControl (LCD_OFF);
	LCD_WriteControl (LCD_CLEAR);
	LCD_WriteControl (LCD_MODE_SET);
	LCD_WriteControl (LCD_ON);
	LCD_WriteControl (LCD_HOME);
}

// ************************************************ //
// *** Clear the LCD screen (also homes cursor) *** //
// ************************************************ //
void LCD_Clear(void)
{
	LCD_WriteControl(0x01);
}


// *********************************************** //
// *** Position the LCD cursor at row 1, col 1 *** //
// *********************************************** //

void LCD_Home(void)
{
	LCD_WriteControl(0x02);
}


/**
@brief Display a character at the row and column previously setted.
@param char : Character to write
@retval void
@note
*/
void LCD_DisplayCharacter (char Char)
{
	LCD_WriteData (Char);
}

// ********************************************************************* //
// *** Display a string at the specified row and column, using FLASH *** //
// ********************************************************************* //
//void LCD_DisplayString_F (char row, char column ,const unsigned char *string)
/*void LCD_DisplayString_F (char row, char column ,const char *string)
{
	LCD_Cursor (row, column);
	while (*string)
		LCD_DisplayCharacter (*string++);
}
*/

/**
@brief Display a string at the specified row and column, using RAM.
@param char : Row definition
@param char : Column definition
@param *string : String to write
@retval void
*/
void LCD_DisplayString (char row, char column ,char *string)
{
	LCD_Cursor (row, column);
	while (*string)
		LCD_DisplayCharacter (*string++);
}

/**
@brief Set row an column.
@param char : Row definition
@param char : Column definition
@retval void
*/
void LCD_Cursor (char row, char column)
{
	switch (row) {
		case 1: LCD_WriteControl (0x80 + column - 1); break;
		case 2: LCD_WriteControl (0xc0 + column - 1); break;
		case 3: LCD_WriteControl (0x94 + column - 1); break;
		case 4: LCD_WriteControl (0xd4 + column - 1); break;

		default: break;
	}
}

// ************************** //
// *** Turn the cursor on *** //
// ************************** //
void LCD_Cursor_On (void)
{
	LCD_WriteControl (LCD_CURS_ON);
}

// *************************** //
// *** Turn the cursor off *** //
// *************************** //
void LCD_Cursor_Off (void)
{
	LCD_WriteControl (LCD_ON);
}

// ******************** //
// *** Turn Off LCD *** //
// ******************** //

void LCD_Display_Off (void)
{
	LCD_WriteControl(LCD_OFF);
}

// ******************* //
// *** Turn On LCD *** //
// ******************* //

void LCD_Display_On (void)
{
	LCD_WriteControl(LCD_ON);
}

/**@{*/