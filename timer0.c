/**
@defgroup gorandi_timer Timer_0
@date 24/05/2016.
@author Ezequiel Gorandi
@version 1.0
@code
#include "timer0.h"
@endcode
@brief
Microcontrolador: Atmega128
*/


/**@{*/

#define _timer0_
#include "Main.H"


TIMERS_INTERNOS Timers_0[CANT_TIMER_0];

uchar TECLA_NUEVA = FALSE;	   //flag
uchar buff_fila1 = 0, buff_fila2 = 0, buff_fila3 =0, buff_fila4 = 0;
uchar buff_teclado = 0;
uchar debounce;
uchar tecla_actual;
uchar tecla_anterior;


/**
@brief   Atención de la interrupción por overflow del timer 0.
@note    
   - Actualiza los timeouts definidos.
   - Recarga la cuenta del timer 0
*/
ISR(TIMER0_OVF_vect)
{
	uchar i;
	/* recargo las cuentas */
	TCNT0 = TIMER0_COUNT;
	/* decremento los timeouts */
	for (i = 0; i < CANT_TIMER_0; i++)
		if(Timers_0[i].Tiempo) Timers_0[i].Tiempo--;
}


/**
@brief   Inicialización del timer 0.
@param   void
@return  void
@note    Configura el timer con los parámetros definidos en timero.h
*/
void init_Timer0(void)
{
   uchar i;

   TCCR0 = TIMER0_TCCR0; //Timer 0 Stop
   ASSR  = TIMER0_SYNC_INTERNAL_MODE; //Sync mode. Internal Clock
   TCNT0 = TIMER0_COUNT; //Set Count
   TIMSK = TIMER0_INTERRUPT_ON; //Enable Timer Interrupt

// All the Timer0 TimeOut = 0
   for(i = 0; i < CANT_TIMER_0; i++) 
   {
      Timers_0[i].Tiempo = 0;
      Timers_0[i].En_uso = 0;
   }
}

/**
@brief   Carga un valor determinado en un timerout.
@param   uchar : timeout que se desea settear.
@param   uchar : valor con el cual desea settearse el timeout.
@return  void
@note    Los timeouts utilizados por el programa, así como los valores con los
         que estos pueden ser setteados, son definidos en timer0.h
*/
void setTimeOut_Timer0(uchar timer, uint valor)
{
   /* cargo el valor de tiempo */
   Timers_0[timer].Tiempo = valor;
   /* activo el uso del timer */
   Timers_0[timer].En_uso = TRUE;
}
/**
@brief   Verifica si se ha vencido un determinado timeout
@param   uchar : timeout que se desea verificar.
@retval  uchar TRUE : Hubo timeout
@retval  uchar FALSE : Aún no hubo timeout
@note    
   - Los timeouts utilizados por el programa, son definidos en timer0.h
   - Previamente debe settearse el timeout utilizando 
     @codevoid setTimeOut_Timer0(uchar timer, uint valor)@endcode
   - Una vez ocurrido el timeout, debe settearse nuevamente esta funcion si se
     quiere que vuelva a ocurrir.
*/
uchar timeOut_Timer0(uchar timer)
{
   if(Timers_0[timer].En_uso && !Timers_0[timer].Tiempo)
   {
      Timers_0[timer].En_uso = FALSE;
      return TRUE;
   }
   return FALSE;
}
/**************************************************************************/

/**************************************************************************
 Prototipo: void Delay_0(uint tiempo)
 Objetivo:  esperar el tiempo especificado
 Recibe:    tiempo
 Devuelve:  
 Notas:
 **************************************************************************/
void delay_Timer0(uint tiempo)
{
   /* cargo el tiempo que me pidieron */
   setTimeOut_Timer0(DELAY_0, tiempo);
   /* espero a que pase */
   while(!timeOut_Timer0(DELAY_0))asm("wdr"); 
}
/**************************************************************************/

/**@{*/